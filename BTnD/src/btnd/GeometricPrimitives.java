/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package btnd;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.JApplet;
import javax.swing.JFrame;

/**
 *
 * @author fieryblaze
 */
public class GeometricPrimitives extends JApplet {
    
    private BufferedImage img;
    
    @Override
    public void init(){
        try{
            URL url = new URL(getCodeBase(), "images/weather-cloud.png");
            img = ImageIO.read(url);
        } catch (IOException e){
            if(e != null)
                System.out.print(e);
        }
        System.out.println(getClass().getClassLoader().getResource("images/weather-cloud.png"));
    }
    
    @Override
    public void paint(Graphics g) { 
        Graphics2D g2 = (Graphics2D) g;
        //Point2D p1 = new Point2D.Double(5,5);
        
        // LINE
        g2.draw(new Line2D.Double(5,5,50,50));
        
        //QuadCurve
        g2.draw(new QuadCurve2D.Double(5, 5, 5, 50, 50, 50));
        
        //CubicCurve
        g2.draw(new CubicCurve2D.Double(5, 5, 25, 25, 50, 5, 50, 50));
        
        //Rectangle
        g2.setPaint(Color.BLUE);
        g2.draw(new Rectangle2D.Double(50, 5, 25, 45));
        
        //Star
        GeneralPath polyline = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
        polyline.moveTo(50, 50);
        polyline.lineTo(75, 100);
        polyline.lineTo(20,65);
        polyline.lineTo(80, 65);
        polyline.lineTo(25, 100);
        polyline.closePath();
        g2.draw(polyline);
        
        //Dashed circle
        float dash[] = {10.0f};
        BasicStroke dashed = 
            new BasicStroke(2.0f,BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL,
                            5.0f, dash, 1.0f);
        Paint fillYellow = new GradientPaint(150,0,Color.YELLOW,300,0,Color.RED);
        g2.setStroke(dashed);
        g2.setPaint(fillYellow);
        g2.fill(new Ellipse2D.Double(200, 200, 100, 100));
        
        //Image
        g2.drawImage(img, 300, 300, null);
        
    }
    
    public static void main(String[] args) {
         JFrame f = new JFrame("Geometric Primitives");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JApplet ap = new GeometricPrimitives();
        ap.init();
        //ap.start();
        f.add("Center", ap);
        f.pack();
        f.setSize(new Dimension(550, 550));
        f.setVisible(true); 
    }
    
}
